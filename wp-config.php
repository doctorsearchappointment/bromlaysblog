<?php
/** Enable W3 Total Cache */
define('WP_CACHE', false); // Added by W3 Total Cache


/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'bromlays_blog');

/** MySQL database username */
define('DB_USER', 'bromlays_blog');

/** MySQL database password */
define('DB_PASSWORD', 'bromlays123');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'Ya*5WhNprrDZ -Y.=W-|gSRXnSY/-gk310cyQjM`tpgC2rjAw`11e8Se``ajS|g;');
define('SECURE_AUTH_KEY',  'y6^WKxNIrL!;^`(.9l.NR=W PHD(dkblarLO#6hNxeW:z~_If6xgVFN5.l/Ll. I');
define('LOGGED_IN_KEY',    '^:_S<U*~H,b^v*P4Z*u~ovmB8/Q3d&qnGG]J$)8L~XT/at=2XF#]oH[f~~?+-rd|');
define('NONCE_KEY',        'wVo4%Cam^E7bu#bcUE#iMYhj>)Bzal_@)?g!;3O>^Z/8V8<M|9jH)!3?g-%e;`>5');
define('AUTH_SALT',        '_NAptq)bRxf}VNY(j%L}d=:KvCUXY6oeUVMv}itTvwk`9ipC5eFh Pe`pE7P&`Y1');
define('SECURE_AUTH_SALT', '|s`!PD(o9HK%|:n!S}mbc~.W/ug6r;A#Ahq@@5ZQa5m3nHj|ICjorAX2/s`zb*/p');
define('LOGGED_IN_SALT',   'P#;#_(p-`q4GWr?~{anAX>{sHsQfLE;^zwwWqw8Z+hO:0f~&cv@@#[xVK:=H?uEo');
define('NONCE_SALT',       'vxT{]CXGd=pB5:MLG)o($0(f(N-2_TUGq,r@l0SF3wJHW:`CYXF1FgX[HY:5WeHo');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'bromlays_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
